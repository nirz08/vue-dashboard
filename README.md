## vue-dashboard

### Description

This is a Vue web app which is made up of features specific to automotive dealer/specialist automation. This includes third part integrations with services from Google, Mailchimp which can be accessed via API's from a back end. It has a secure loigin section which uses Auth0 for authentication using web tokens.

### /api

All third part integrations are accessed via `axios` using a web API and designed to use a Factory/Repository style pattern where all authentication with Auth0 is handled in the base Repository class which is then extended to individual repositories per external API. Each API has it's own sub-folder based on the service it provides. Error handling for all API calls are thrown directly to the calling function.

### /auth

Client-side authentication is handled using the `auth0.js` library. Users are redirected from the route domain to a custom auth0 login page. Once credentials are successfully provided they are redirected back to the app with a token which corrosponds to the Auth0 tennancy which was used to login. This token + user credential/payload is then stored inside the Vuex user state. It's used in two main places:

1. Used to ensure the local session is authenticated, otherwise force a logout/redirect back to the root path.
2.  Used/refreshed upon each request to the external backend where the user credential is checked at the API gateway level to ensure all request are authenticated when they are processed by the back end.

### /components

Each main feature is broken down into a sub-folder of Vue single file components and corrosponding unit tests. Components can only interact with their parent, child and the Vuex store.

This includes:

- Campaign Generation
- Internal Tools

### /router

Standard Vue router. A single `beforeEach()` guard is used to handle authentication redirects.

### /store

The Vuex store is broken down using a module pattern for each corrosponding feature. Each module is then responsible for that feature's state and the `mutations` for that state. As well as `actions` which make the appropriate API calls to their API repository. 

Error handling for each action is handled inside the corrosponding function which informs the user via feedback from popup/toast modals here. There is two main types of errors.

1. API errors from bad requests which are handled based on status code which should likely fail gracefully and provide user feedback.
2. Action level errors which should fail hard and throw errors to the action calling component.

### /styles

Contains any custom `sass` ,`scss` for the project and also overwrites some of the default CSS framework (Bulma/Buefy) classes. 

### /utils

Misc functions

### /views

The main components for the app that are not captured as a feature.

## How to Run

**LocalHost**

To run: `npm run start`- Launches on `http://APP_HOST:APP_PORT`

To run tests: `npm run test` - Launches Jest test suite

To build: `npm run build` - Builds contents of `/dist` folder

**BitBucket Pipelines**

To run tests: `npm run test-pipelines` - Uses custom config to avoid Jest from stopping pipeline steps



