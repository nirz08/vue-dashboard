#!/usr/bin/env bash

echo "This is after Install"

rsync -avhc --chown=nginx:nginx --delete-after --exclude='appspec.yml' --exclude='codedeployment' /var/www/release/ /var/www/html/

cd /var/www/html/

mkdir config
mkdir -p src/server/config
aws s3 cp s3://da-frontend/.env .env
aws s3 cp s3://da-frontend/config ./config/ --recursive
aws s3 cp s3://da-frontend/config ./src/server/config/ --recursive

sudo npm install
sudo npm run build
rm -rf ./src/client
runuser -l ec2-user -c 'pm2 restart www --node-args="-r esm"'

systemctl restart nginx