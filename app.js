var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var rfs = require('rotating-file-stream')
var index = require('./src/server/routes');
var dealersRouter = require('./src/server/routes/dealers');
var vmsRouter = require('./src/server/routes/vms');
var mailchimp = require('./src/server/routes/mailchimp')
var gmb = require('./src/server/routes/gmb')
var clientPortal = require('./src/server/routes/client_portal')
var mongo = require( './src/server/database' );
var cors = require('cors');
var app = express();
app.use(cors({
  origin: "*",
  optionsSuccessStatus: 200
}));

// create a rotating write stream
var accessLogStream = rfs('access.log', {
  interval: '1d', // rotate daily
  path: path.join(__dirname, 'log')
})

// setup the logger
app.use(morgan('combined', {
   stream: accessLogStream,
   skip: function (req, res) { return res.statusCode < 400 } 
  }))

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/dealers', dealersRouter);
app.use('/api/mailchimp', mailchimp)
app.use('/api/gmb', gmb)
app.use('/api/client_portal', clientPortal)
app.use('/api/vms', vmsRouter)
if('production' == process.env.NODE_ENV) {
  app.use(express.static(path.join(__dirname, 'dist')));
}
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = process.env.NODE_ENV === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  return res.json(err.message ? {err:err.message} : {err});
});

module.exports = app;
