The Vuex store is the single source of truth for state management in application. It contains modules for each piece of data
that needs to be shared globally across the application. The reason why this seemingly complicated system exists is to make sure
that data persists across all pages in the same way and changes on one page will reflect accross all Vue components. This is super important!!!

The key components of the state management system are as follows:

1. An index file contains the main "store" for the data which is shared across Vue components. It has to be imported into every file as well as
the "mapState" function which helps view the state.

2. There needs to be a module for each thing that must be tracked globally accross all routes/pages, ie: which dealership/date range settings.

3. Modifying the data's state occurs by performing mutations on the data. Mutations directly change the "state" object properties.

4. To perform a mution you need to call an "Action" function which is passed the name of the mutation and a payload (which is used to change the state).

When this is implemented we don't have to worry about state being mismatched across the application, you just have to follow the steps outlined in the 
existing modules to make it work!

Breakdown:

index.js - this is the Vuex store file and all the modules get imported here
modules
|-- dealer.js- Dealer's state is all maintained inside, this includes all actions and mutations which can occur on it.

