// src/store/__mocks__/index.js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const mutations = {
    SET_USERNAME: jest.fn(),
    SET_FULLNAME: jest.fn(),
    SET_EMAIL: jest.fn(),
    SET_TOKEN: jest.fn(),
    SET_AUTHENTICATED: jest.fn(),
    SET_ROLES: jest.fn(),
    RESET: jest.fn(),
    SET_DEALERS: jest.fn(),
    SET_ODIN_DEALERS: jest.fn(),
    SET_OEM_GROUPS: jest.fn(),
    GET_USERS: jest.fn()
};

export const actions = {
    loginByJWT: jest.fn(),
    getDealers: jest.fn(),
    reset: jest.fn(),
    getOdinDealers: jest.fn(),
    updateOEMGroups: jest.fn(),
    getUsers: jest.fn(),
    getAudiences: jest.fn(),
    schedPost: jest.fn()
};

export const state = {
    gmb_data: {
        localPosts: [],
        location: {},
        locations: [],
        location_ids: [],

    },
    user: {
        authenticated: true,
        dealerId: '',
        dealers: [],
        email: "nrizinger@test.com",
        fullName: "Noah Irzinger",
        odin_dealers: [
           
        ],
        activeOemDealers: [

        ],
        roles: [],
        settings: {},
        token: '',
        userName: "nirzinger",
        users: [{
                "created_at": "2020-02-11T00:17:00.897Z",
                "email": "nrizinger@test.com",
                "email_verified": false,
                "family_name": "Irzinger",
                "given_name": "Noah",
                "identities": [{
                    "user_id": "5e41f27c138d1e0e9590b8cc",
                    "provider": "auth0",
                    "connection": "dealerportal-dev",
                    "isSocial": false
                }],
            }
        ]
    },
    odin_dealer: {
        data: {
           
        },
        loading: false,
        updatingDealer: false,
        dealerSet: false,
    },
    mailchimp: {
        mailchimp_settings: {
            

        }
    }
};

export function __createMocks(custom = {
    mutations: {},
    actions: {},
    state: {}
}) {
    const mockMutations = Object.assign({}, mutations, custom.mutations);
    const mockActions = Object.assign({}, actions, custom.actions);
    const mockState = Object.assign({}, state, custom.state);

    return {
        mutations: mockMutations,
        actions: mockActions,
        state: mockState,
        store: {
            mutations: mockMutations,
            actions: mockActions,
            state: mockState,
        },
    };
}

export const store = __createMocks().store;