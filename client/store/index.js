import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import odin_dealer from './modules/odin_dealer.js'
import persistState from 'vuex-persistedstate'
import gmb_data from './modules/gmb_data.js'
import semrush_data from './modules/semrush_data.js'
import vms_data from './modules/vms_data.js'
import mailchimp from './modules/mailchimp.js'
import loading from './modules/loading.js'
Vue.use(Vuex)
var store = new Vuex.Store({
  namespaced: true,
  modules: {
    user,
    odin_dealer,
    gmb_data,
    semrush_data,
    vms_data,
    mailchimp,
    loading
  },
  plugins: [persistState()],
  actions: {
    async logout({dispatch}) {
      dispatch('user/reset')
      dispatch('odin_dealer/reset')
      dispatch('gmb_data/reset')
      dispatch('vms_data/reset')
      dispatch('semrush_data/reset')
      dispatch('mailchimp/reset')
      dispatch('loading/reset')
    }
  }
})
export default store