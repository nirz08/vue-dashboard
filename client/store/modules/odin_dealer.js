import { ToastProgrammatic as Toast } from 'buefy'
import Factory from '@api/Factory'
const DealerRepository = new Factory('dealer')

function getInitialState() {
    return {
        data: {},
        oemGroups: [],
        activeOemDealers: [],
        loading: false,
    }
}

const odin_dealer = {
    namespaced: true,
    state: getInitialState(),
    mutations: {
        // replaces entire dealer state with updated one
        SET_DEALER_DATA: (state, data) => {
            return Object.assign(state, { data })
        },
        SET_OEM_GROUPS: (state, oemGroups) => {
            Object.assign(state, { oemGroups });
        },
        SET_ACTIVE_OEM_DEALERS: (state, activeOemDealers) => {
            Object.assign(state, { activeOemDealers });
        },
        RESET: (state) => {
            Object.assign(state, getInitialState())
        }
    },
    actions: {
        async reset({ commit, state }) {
            try {
                state.loading = false
                commit('RESET')
            } catch (error) {
                throw error
            }
        },
        /**
         * Retrieves the dealer data via an api call, sets the dealer returned as the odin_dealer
         * @param {*} param0 
         * @param {String} id -  A valid dealer ObjectId
         * @returns {Object} - The data from the response object (dealer data)
         */
        async getOdinDealer({ commit, state }, id) {
            try {
                state.loading = true
                const response = await DealerRepository.getDealer(id)
                if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
                if (response.status === 401) throw new Error('Unauthorized Error', response)
                if (response.status != 200) throw new Error('Getting odin_dealer failed', response)
                const dealer = response.data
                commit('SET_DEALER_DATA', dealer)
                state.loading = false
                return response.data
            } catch (error) {
                state.loading = false
                errorToast(error)
                throw error
            }
        },
        /**
         * Create a new dealer in the dealer collection, also dispatches the `user/getOdinDealers`
         * action to update the dealer list in the user object
         * @param {*} param0 
         * @param {Object} data - The fields required to make a new dealer
         * @returns {Object} - The response data from the service, usually something like {nInserted: 1}
         */
        async createOdinDealer({ dispatch, state }, data) {
            try {
                state.loading = false
                const response = await DealerRepository.createDealer(data)
                await dispatch('user/getOdinDealers', {}, {root: true})
                state.loading = false
                if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
                if (response.status === 401) throw new Error('Unauthorized Error', response)
                if (response.status === 409) throw new Error('Failed Duplicate Dealer Found', response)
                if (response.status !== 200) throw new Error('Failed to Create New Dealer', response)
                successToast({message: 'Created New Dealer'})
                return response.data
            } catch (error) {
                state.loading = false
                errorToast(error)
                throw error
            }
        },
        /**
         * Updates a given dealer in the database given the payload, also dispatches the `user/getOdinDealers` action to update the odin_dealers list
         * @param {*} param0 
         * @param {*} payload - Contains the id and the data for the dealer to update
         * @returns {Object} - The data from the updateDealer api call
         */
        async updateOdinDealer({ dispatch, state }, payload) {
            try {
                state.loading = true
                const response = await DealerRepository.updateDealer(payload.id, payload.data)
                if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
                if (response.status === 401) throw new Error('Unauthorized Error', response)
                if (response.status !== 200) throw new Error('Updating odin_dealer failed', response)
                await dispatch('user/getOdinDealers', {}, {root: true})
                state.loading = false
                successToast({message: 'Updated Dealer'})
                return response.data
            } catch (error) {
                state.loading = false
                errorToast(error)
                throw error
            }
        },
        /**
         * Deletes a dealer given the id
         * @param {*} param0 
         * @param String} id - the dealer Id to delete
         * @returns {Object} - The response from the deleteDealer api call 
         */
        async deleteOdinDealer({ dispatch, state }, id) {
            try {
                state.loading = true
                const response = await DealerRepository.deleteDealer(id)
                await dispatch('user/getOdinDealers', {}, {root: true})
                state.loading = false
                if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
                if (response.status === 401) throw new Error('Unauthorized Error', response)
                if (response.status != 200 || response.data.nRemoved < 1) throw new Error('Deleting odin_dealer failed', response)
                successToast({message: 'Deleted Dealer'})
                return response
            } catch (error) {
                state.loading = false
                errorToast(error)
                throw error
            }
        },
        //  TODO - these are funky we need to update logic for OEM groups in the future
        /**
         * Gets the oemGroups via the api call to getOemGroups, updates the oemGroups state with the list
         * @param {*} param0 
         * @returns {Object} oemGroups - The response from the getOemGroups api call
         */
        async getOemGroups({commit, state}) {
            try {
                state.loading = true
                const response = await DealerRepository.getOemGroups()
                if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
                if (response.status === 401) throw new Error('Unauthorized Error', response)
                if (response.status != 200) throw new Error('Getting odin_dealer failed', response)
                const oemGroups = response.data
                commit('SET_OEM_GROUPS', oemGroups)
                state.loading = false
                return oemGroups
            } catch (error) {
                state.loading = false
                errorToast(error)
                throw error
            }
        },
        /**
         * Sets the active oemGroup
         * @param {*} param0 
         * @param {*} payload 
         */
        async setActiveOEMDealers({commit, state}, payload) {
            try {
                state.loading = true
                commit('SET_ACTIVE_OEM_DEALERS', payload)
                state.loading = false
            } catch(error) {
              throw error;
            }
        },
    
    },
};

function successToast(data) {
    Toast.open({
    duration: 6000,
    message: `${data.message}`,
    type: "is-primary",
    position: "is-bottom"
    })
}
function errorToast(data) {
    Toast.open({
    duration: 12000,
    message: `${data.message}`,
    type: "is-danger",
    position: "is-bottom"
    })
}

export default odin_dealer;