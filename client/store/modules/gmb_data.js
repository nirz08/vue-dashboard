import Vue from 'vue'
import { ToastProgrammatic as Toast } from 'buefy'
import Factory from '@api/Factory'
const PostsRepository = new Factory('posts')

function getInitialState() {
  return {
    location_ids: [],
    location: {
      media: [{ yeet: 1 }],
      localPosts: []
    },
    mediaSet: false,
    loadingMedia: false,
    loadingPosts: false,
    loadingLocation: false,
    loadingReviews: false,
    loadingAllLocations: false,
    pendingPostUploads: [],
    savedPosts: [],
    postContent: [],
    activePostContent: [],
    // New service states
    localPosts: [],
  }
}

const gmb_data = {
  namespaced: true,
  state: getInitialState(),
  getters: {
    gmbImages: state => {
      let gmbImages = state.location.media;
      return gmbImages;
    }
  },
  mutations: {
    SET_LOCATION_IDS: (state, location_ids) => {
      Object.assign(state, { location_ids })
    },
    SET_LOCATIONS: (state, locations) => {
      Object.assign(state, { locations })
    },
    SET_LOCAL_POSTS: (state, localPosts) => {
      Object.assign(state.location, { localPosts })
      // New service mutation...
      Object.assign(state, { localPosts })
    },
    SET_LOCATION: (state, location) => {
      Object.assign(state, { location })
    },
    SET_TOKEN: (state, token) => {
      Object.assign(state, { token })
    },
    SET_MEDIA: (state, media) => {
      Object.assign(state.location, { media })
    },
    SET_REVIEWS: (state, reviews) => {
      Object.assign(state.location, { reviews })
    },
    SET_AVG_REVIEW_SCORE: (state, avg_review_score) => {
      Object.assign(state.location, { avg_review_score })
    },
    SET_SAVED_POSTS: (state, savedPosts) => {
      Vue.set(state, 'savedPosts', [...savedPosts])
    },
    SET_CATEGORIES: (state, categories) => {
      Object.assign(state, { categories })
    },
    RESET: (state) => {
      Object.assign(state, getInitialState())
    },
    SET_POST_CONTENT: (state, postContent) => {
      Object.assign(state, { postContent })
    },
    SET_ACTIVE_POST_CONTENT: (state, activePostContent) => {
      Object.assign(state, { activePostContent })
    }
  },
  actions: {

    /**
     * Retrieves the localPosts for the location
     * @param {String} id -  A valid location ObjectId - not used right now, todo?
     * @returns {Object} - The data from the response object (localpost data)
     */
    async getLocalPosts({ commit, state }, data) {
      try {
        state.loadingPosts = true
        // We may need to get posts for dealer locations or a whole oem...
        let params
        if (typeof data === 'undefined') {
          //  Handle get by locations
          //  Why tf does the location state not have it's own objectId?! :( Do this for now...
          const found = state.locations.find(location => location.name === state.location.name)
          const id = found._id
          //  Get the posts
          params = { locationId: id }
        } else {
          // Handle get by OEMs
          const oem = data.oem
          params = { oem: oem }
        }

        const response = await PostsRepository.getPosts(params)
        if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
        if (response.status != 200) throw new Error('Getting localPosts failed', response)
        const localPosts = response.data
        //  Set the posts state
        commit('SET_LOCAL_POSTS', localPosts)
        state.loadingPosts = false
        return response.data
      } catch (error) {
        state.loadingPosts = false
        // errorToast(error)
        throw error
      }
    },
    /**
     * Schedules a new post
     * @param {Object} data - A post object 
     */
    async schedulePost({ commit, dispatch, state }, data) {
      try {
        const response = await PostsRepository.schedulePost(data)
        if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
        if (response.status != 200) throw new Error('Scheduling post failed', response)
        successToast({ message: 'Created New Post' })
        return response.data
      } catch (error) {
        errorToast(error)
        throw error
      }
    },
    /**
     * Delete a localPost
     * @param {String} postId - The localPost ObjectId 
     */
    async deletePost({ dispatch }, postId) {
      try {
        const response = await PostsRepository.deletePost(postId)
        if (typeof response === 'undefined' || response.status === 500) throw new Error('Server Error', response)
        if (response.status != 200) throw new Error('Deleting post failed', response)
        successToast({ message: 'Deleted Post' })
        return response.data
      } catch (error) {
        errorToast(error)
        throw error
      }
    },
  }
}
function successToast(data) {
  Toast.open({
    duration: 6000,
    message: `${data.message}`,
    type: "is-primary",
    position: "is-bottom"
  })
}
function errorToast(data) {
  Toast.open({
    duration: 12000,
    message: `${data.message}`,
    type: "is-danger",
    position: "is-bottom"
  })
}
export default gmb_data;