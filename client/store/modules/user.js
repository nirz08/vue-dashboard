import { loginByJWT, getDealers, validateJWT } from '../../api/wp-rest.js';
import DealerHandler from '../../api/DealerHandler'
import UserHandler from '../../api/UserHandler'
import API from '../../utils/axios';
function getInitialState() {
  return {
    userName: '',
    fullName: '',
    email: '',
    token: '',
    authenticated: false,
    settings: {}, // TODO - all the client side user settings
    dealers: [],
    odin_dealers:[],
    roles: [],
    dealerId: '',
    oemGroups: [],
    activeOemDealers: [], 
    users: [],
  }
}
const user = {
  namespaced: true,
  state: getInitialState(),
  mutations: {
    SET_USERNAME: (state, userName) => {
      Object.assign(state, { userName });
    },
    SET_FULLNAME: (state, fullName) => {
      Object.assign(state, { fullName });
    },
    SET_EMAIL: (state, email) => {
      Object.assign(state, { email });
    },
    SET_TOKEN: (state, token) => {
      Object.assign(state, { token });
    },
    SET_AUTHENTICATED: (state) => {
      state.authenticated = true;
    },
    SET_ROLES: (state, roles) => {
      Object.assign(state, { roles })
    },
    RESET: (state) => {
      Object.assign(state, getInitialState());
    },
    SET_DEALERS: (state, dealers) => {
      Object.assign(state, { dealers });
    },
    SET_ODIN_DEALERS: (state, odin_dealers) => {
      Object.assign(state, { odin_dealers });
    },
    SET_USERS: (state, users) => {
      Object.assign(state, { users });
    },
    SET_OEM_GROUPS: (state, oemGroups) => {
      Object.assign(state, { oemGroups });
    },
    SET_ACTIVE_OEM_DEALERS: (state, activeOemDealers) => {
      Object.assign(state, { activeOemDealers });
    },
    SET_USER: (state, user) => {
      let index = state.users.findIndex(d => d.user_id === user.user_id)
      state.users[index] = user
    }
  },

  actions: {
    async auth0Login({commit}, data) {
      commit('SET_TOKEN', data.accessToken)
      commit('SET_FULLNAME', data.user.name)
      commit('SET_EMAIL', data.email)
      commit('SET_ROLES', data.roles)
      commit('SET_AUTHENTICATED')
      commit('SET_USERNAME', data.user.nickname)
    },
    reset({ commit }) {
      try {
        commit('RESET');
      } catch (error) {
        return error;
      }
    },
    /**
     * Attempt to logout via auth0
     * @param {*} param0 
     * @param {Object} router - Vue Router object 
     */
    async auth0Logout({dispatch}, router) {
      try {
        await router.app.$auth.logout()
        await dispatch('logout', null, {root: true})
      } catch (error) {
        throw (error)
      }
    },
    /**
     * Retrieve a list of dealers from the dealer handler service
     * @param {*} param0 
     * @param {Object} params - An key/value pair set of query params for the api call 
     */
    async getOdinDealers({commit}, params) {
      try {
        const response = await DealerHandler.getDealers(params)
        const dealers = response.data
        commit('SET_ODIN_DEALERS', dealers)
        return response
      } catch (error) {
        throw error
      }
    },
    /**
     * Returns the user list from auth0
     * @param {*} param0 
     */
    async getUsers({commit}) {
      try {
        let response = await UserHandler.getUsers()
        commit('SET_USERS', response)
        return response
      } catch (error) {
        throw error
      }
    },
    /**
     * Updates the oem group collection 
     * @param {*} param0 
     * @param {Object} payload - An updated oem group object from the component
     */
    async updateOEMGroups({commit}, payload) {
      try {
        let response
        if (payload) {
          let id = payload._id
          delete payload._id
          delete payload.dealer_business_name
          response = await DealerHandler.updateDealer(id, payload)
        }
        return response
      } catch (error) {
        throw error
      }
    },
    /**
     * Return the oem groups as a list
     * @param {*} param0 
     */
    async getOEMDealerGroups({commit}) {
        try {
          let response = await DealerHandler.getDealerOEMGroups()
          commit('SET_OEM_GROUPS', response.data)
        } catch(error) {
          throw error;
        }
    },
    /**
     * Sets the oem group 'active' flag on/off for a specified dealer
     * @param {*} param0 
     * @param {*} payload 
     */
    async setActiveOEMDealers({commit}, payload) {
        try {
          commit('SET_ACTIVE_OEM_DEALERS', payload)
        } catch(error) {
          throw error;
        }
    },
    /**
     * Remove a dealer from the auth0 user store
     * @param {*} param0 
     * @param {*} payload 
     */
    async deleteActiveDealer({commit}, payload) {
        try {
          console.log(activeOemDealers);
          commit('SET_ACTIVE_OEM_DEALERS', payload)
        } catch(error) {
          throw error;
        }
    },
    /**
     * Currently this action only updates the user_metadata property on the Auth0 user object
     * and then updates the state to store that returned user if successful
     * @param {*} param0
     * @param {Object} payload - The user object to be updated
     * @thows - If the API call fails or updating the state fails
     */
    async updateUser({commit}, payload) {
      try {
        //  atempt to update dealers with new data
        let response = await UserHandler.updateUserVMSAssignedDealers(payload.user.sub, payload)
        if (response.status !== 200) throw new Error('updateUser failed', response)
        const user = response.data
        commit('SET_USER', user)
        return response
      } catch (error) {
        throw error
      }
    }
  }
}

export default user;