import CampaignHandler from '../../api/CampaignHandler'
import { ToastProgrammatic as Toast } from 'buefy'

function getInitialState() {
  //setup some data here xd
  return {
    templates: [],
    audiences: [],
    unscheduledEmails: [],
    uploading: false,
  }
}

const monkey = {
  namespaced: true,
  state: getInitialState(),
  getters: {

  },
  mutations: {
    SET_AUDIENCES: (state, audiences) => {
      Object.assign(state, { audiences })
    },
    SET_TEMPLATES: (state, templates) => {
      Object.assign(state, { templates })
    },
    SET_MAILCHIMP_SETTINGS: (state, mailchimp_settings) => {
      Object.assign(state, {mailchimp_settings})
    },
    SET_UNSCHEDULED_EMAILS: (state, email_list) => {
      Object.assign(state, { unscheduledEmails: email_list })
    },
    RESET: (state) => {
      Object.assign(state, getInitialState())
    }
  },
  actions: {

    async reset({commit}) {
      try {
        commit('RESET');
      } catch (error) {
        return error
      }
    },
    /**
     * Returns the email template as an xlsx file 
     * @param {*} param0
     */
    async downloadEmailTemplate({}) {
      try {
        let response = await CampaignHandler.downloadEmailTemplate()
        return response
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Returns the response from the validate endpoint with a payload of validated emails
     * @param {*} param0 
     * @param {*} payload 
     */
    async validateEmailTemplate({}, payload) {
      try {
        let response = await CampaignHandler.validateEmailTemplate(payload)
        return response
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Returns the audience list from mailchimp
     */
    async getAudiences() {
      try {
        let response = await CampaignHandler.getAudiences()
        return response.data
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Updates a specific audience given a payload
     * @param {*} param0 
     * @param {*} payload - The audience data to update id/data
     */
    async updateAudience({}, payload) {
      try {
        let response = await CampaignHandler.updateAudience(payload.id, payload.data)
        return response.data
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Return the custom audience segments from the campaign generation service
     * @param {*} param0 
     * @param {*} payload 
     */
    async getAudienceSegments({}, payload) {
      try {
        let response = await CampaignHandler.getAudienceSegments(payload)
        return response.data
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Returns all the active email templates from mailchimp
     */
    async getCampaignTemplates() {
      try {
        let response = await CampaignHandler.getCampaignTemplates()
        return response.data
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Schedules a given campaign in the database
     * @param {*} param0 
     * @param {*} payload - A campaign type object as per the mailchimp spec
     */
    async scheduleCampaigns({}, payload) {
      try {
        var response = await CampaignHandler.scheduleCampaigns(payload)
        if (typeof response === "undefined" || response.status != 200) throw new Error(`Scheduling campaigns failed`, response)
        successToast({
          message: 'Scheduled Campaigns Successfully'
        })
        return response
      } catch ( error ) {
        errorToast(error)
        throw error
      }
    },
    /**
     * Returns the mailchimp settings for a specific dealer in the database 
     * @param {*} param0 
     * @param {*} payload - An ObjectId for the dealer
     */
    async getSettings({commit}, payload) {
      try {
        let response = await CampaignHandler.getSettings(payload.id)
        commit("SET_MAILCHIMP_SETTINGS", response.data);
        return response.data
      } catch ( error ) {
        throw error
      }
    },
    /**
     * Updates the mailchimp settings for a specific user
     * @param {*} param0 
     * @param {*} payload - The settings object with updated values
     */
    async updateSettings({}, payload) {
      try {
        let response = await CampaignHandler.updateSettings(payload.id, payload.data)
        if (typeof response === "undefined" || response.status != 200) throw new Error(`Failed to store ${payload.type} settings. Attempting to schedule`, response)
        return response.data
      } catch ( error ) {
        errorToast(error)
        throw error
      }
    },
  }
}
function successToast(data) {
  Toast.open({
    duration: 6000,
    message: `${data.message}`,
    type: "is-primary",
    position: "is-bottom"
  })
}

function errorToast(data) {
  Toast.open({
    duration: 12000,
    message: `${data.message}`,
    type: "is-danger",
    position: "is-bottom"
  })
}

export default monkey;