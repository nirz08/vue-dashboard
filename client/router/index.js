import Vue from 'vue'
import Router from 'vue-router'
// import Welcome from '../views/Welcome.vue'
import Secure from '@views/Secure.vue'
import Welcome from '@views/Welcome.vue'
import SignIn from '@views/SignIn.vue'
import SEOManager from '@views/managers/SEOManager.vue'
import InternalTools from '@views/managers/InternalTools.vue'
import DealerManager from '@views/managers/DealerManager.vue'
import Dashboard from '@views/dashboards/Dashboard.vue'
import OrganicAnalytics from '@views/dashboards/OrganicAnalytics.vue'
import PaidAnalytics from '@views/dashboards/PaidAnalytics.vue'
import Profile from '@views/user/Profile.vue'
import Settings from '@views/user/Settings.vue'
import Help from '@views/user/Help.vue'
import store from "store/index.js";
import Callback from '../auth/Callback.vue'
Vue.use(Router)



var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,

  routes: [
    {
      path: '/',
      redirect: {
        name: "Welcome"
      },
      meta: {
        requiresAuth: false,
        requiresDealer: false
      }
    },
    {
      path: '/callback',
      name: 'callback',
      component: Callback
    },
    {
      path: '/login',
      name: 'login',
      component: SignIn,
      meta: {
        requiresAuth: false,
        requiresDealer: false
      }
    },
    {
      path: "/secure",
      name: "",
      component: Secure,
      meta: {
        requiresAuth: true,
        requiresDealer: false
      },
      children: [
        {
          path: 'welcome',
          name: 'Welcome',
          component: Welcome,
          meta: {
            requiresAuth: true,
            requiresDealer: false
          }
        },
        {
          path: 'seo',
          name: 'SEO Manager',
          component: SEOManager,
          meta: {
            breadcrumb: [
              { name: 'SEO Manager', link: 'seo' },
            ],
            requiresAuth: true,
            requiresDealer: false
          }
        },
        {
          path: 'tools',
          name: 'Internal Tools',
          component: InternalTools,
          meta: {
            requiresAuth: true,
            requiresDealer: false,
            breadcrumb: [
              { name: 'Internal Tools', link: 'tools' }   //No link here because this is current page
            ]
          },
        },
        {
          path: 'dealer',
          name: 'Dealer Manager',
          component: DealerManager,
          meta: {
            requiresAuth: true,
            requiresDealer: true,
            breadcrumb: [
              { name: 'Dealer Manager', link: 'dealer' }   //No link here because this is current page
            ]
          }
        },
        {
          path: '/settings',
          name: 'User Settings',
          component: Settings
        },
      ]
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
      path: '/organic',
      name: 'Organic Analytics',
      component: OrganicAnalytics,
      //Add these to display breadcrumbs
      meta: {
        breadcrumb: [
          { name: 'Dashboard', link: 'dashboard' },
          { name: 'Organic Analytics' }   //No link here because this is current page
        ]
      }
    },
    {
      path: '/paid',
      name: 'Paid Analytics',
      component: PaidAnalytics,
      meta: {
        breadcrumb: [
          { name: 'Dashboard', link: 'dashboard' },
          { name: 'Paid Analytics' }
        ]
      }
    },

    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/help',
      name: 'help',
      component: Help
    },
    {
      path: '*',
      redirect: {
        path: "/secure/welcome"
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  if(to.name == 'callback') { // check if "to"-route is "callback" and allow access
    next()
  } else if (router.app.$auth.isAuthenticated()) { // if authenticated allow access
    next()
  } else { // trigger auth0 login
    router.app.$auth.login()
  }
})

export default router;
