import axios from 'axios'
import store from 'store/index'
const API = axios.create()

API.interceptors.request.use(
  (serviceConfig) => {
    if (store.state.user.token) {
      serviceConfig.headers.Authorization = 'Bearer ' + store.state.user.token;
    }
    return serviceConfig;
  },
  (error) => {
    Promise.reject(error);
  },
);

// response interceptor
API.interceptors.response.use(
  (response) => {
    const res = response;
    if (res.status !== 200) {
      return res.message;
      // return Message({
      //   message: res.message,
      //   type: 'error',
      //   duration: 5 * 1000,
      // });
    }
    return res;
  },
  (error) => {
    return error.response;
    //   Message({
    //     message: error.message,
    //     type: 'error',
    //     duration: 5 * 1000,
    //   });
    //   return Promise.resolve(error);
  },
);
export default API;