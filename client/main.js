import Vue from 'vue'
import App from './App.vue'
import store from './store/index.js'
import router from './router'
import cors from 'cors'
import lodash from 'lodash';
import Buefy from 'buefy'
import "@mdi/font/css/materialdesignicons.css";
import StarRating from 'vue-star-rating'; 
import auth from './auth'
import './plugins';
import EB from './eventBus'
// import 'buefy/dist/buefy.css' // comment out in order to override bulma defaults with sass in App.js
Vue.use(Buefy);
// Vue.use(Bulma);
Vue.use(lodash);
// used to handle closing components when clicking outside of their element
import vClickOutside from 'v-click-outside';
Vue.use(vClickOutside);
// Initialize auth module
Vue.use(auth)

Vue.use(cors);

Vue.config.productionTip = false
Vue.component('star-rating', StarRating);

export const eventBus = EB

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


if (module.hot) {
  module.hot.accept();
  module.hot.dispose(function() {
    clearInterval(timer);
  });
}