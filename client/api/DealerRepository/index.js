import Repository from '@api/Repository'

export default class DealerRepository extends Repository {
    constructor() {
        super()
    }
    async getDealer(id, params={}) {
        const paramsString = new URLSearchParams(params).toString()
        const response = await this.request.get(`/v0/dealerships/${id}?${paramsString}`)
        return response
    }
    async createDealer(data) {
        const response = await this.request.post(`/v0/dealerships`, data)
        return response
    }
    async updateDealer(id, data) {
        let response = await this.request.patch(`/v0/dealerships/${id}`, data)
        return response
    }
    async deleteDealer(id) {
        const response = await this.request.delete(`/v0/dealerships/${id}`) 
        return response
    }
    async getDealers(params={}) {
        const paramsString = new URLSearchParams(params).toString()
        const response = await this.request.get(`/v0/dealerships?${paramsString}`)
        return response
    }
    async getOemGroups() {
        const response = await this.request.get(`v0/dealerships/oem`)
        return response
    }
}