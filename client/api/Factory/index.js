import DealerRepository from '@api/DealerRepository'
import PostsRepository from '@api/PostsRepository'

export default class Factory {
    constructor(name) {
        this.repositories = {
            dealer: new DealerRepository(),
            posts: new PostsRepository(),
        }
        return this.repositories[name]
    }
}