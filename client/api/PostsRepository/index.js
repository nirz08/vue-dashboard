import Repository from '@api/Repository'

export default class PostsRepository extends Repository {
    constructor() {
        super()
    }
    async getPosts(params={}) {
        const paramsString = new URLSearchParams(params).toString()
        const response = await this.request.get(`/v0/posts?${paramsString}`)
        return response
    }
    async schedulePost(data) {
        const response = await this.request.post(`/v0/posts`, data)
        return response
    }
    async deletePost(id) {
        const response = await this.request.delete(`/v0/posts/${id}`)
        return response
    }
}