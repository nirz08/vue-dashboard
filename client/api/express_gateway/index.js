import axios from 'axios';
export default class ExpressGatewayHandler {

    constructor() {
        this.store = require('../../store')
        this.request = axios.create({
            baseURL: process.env.EXPRESS_GATEWAY_URL
        })
        this.request.interceptors.request.use(
            (serviceConfig) => {
                var store = require('../../store')
                serviceConfig.headers.Authorization = 'Bearer ' + store.default.state.user.token;
                serviceConfig.validateStatus = (status) => {
                    return status >= 200 && status < 401
                }
                return serviceConfig;
            },
            (error) => {
                throw(error)
            },
        );
        this.request.interceptors.response.use(
            (response) => {
                if(response.status == 401) {
                    console.log('error')
                }
                return response
            },
            (error) => {
                Promise.reject(error)
            }
        )
    }

}