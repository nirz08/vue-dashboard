import axios from 'axios'
import store from '../../store'

export default class Repository {
    constructor() {
        this.request = axios.create({
            validateStatus: (status) => {
                return true; // Never throw errors when a response is returned
              },
        })
        this.request.interceptors.request.use(
            (serviceConfig) => {
                serviceConfig.headers.Authorization = `Bearer ${store.state.user.token}`
                serviceConfig.baseURL = process.env.EXPRESS_GATEWAY_URL
                return serviceConfig
            },
            (error) => {
                Promise.reject(error.response)
            },
        );
        this.request.interceptors.response.use(
            (response) => {
                return response
            },
            (error) => {
                Promise.reject(error.response)
            }
        )
    }
}