# OEM Post Automation

## Components

### OEMPostAutomation.vue

**Description**
The OEM Post Automation tab is used to schedule local posts on an OEM-wide scale. This tab displays: 

 - The oem list that expand into meta data input fields for posts
 - Expandable OEM cards which hold all dealers within the oem for selection
 - UTM Builder for creating tags to append on post CTA links.

**Tests**

**Data**

- `selectedOEMS` - An `Array` of OEMs with entered post meta data
- `currentOEM` - An OEM `Object` emitted from OEMDealerSelect.vue. Used to retreive an OEM group after modifying its fields in the DB.
- `fields` - an `Array` of local post meta data `Objects`. An object is pushed into this array for each OEM with entered post meta data.

**Computed**

- `oemGroup` - An `Array`of dealers from an OEM, matched to `currentOEM` data attribute. Loaded from Vuex store`s state.
- `user` - An `Object` of currently logged in user loaded from Vuex store`s state.
- `locations` - An `Array` of all GMB locations loaded from Vuex store`s state.

**Methods**

- `addFields(event)` - Adds an OEMs post meta data `Object`. Values entered and emitted from `OEMGroupCard`.
- `getFields(oem)` - Returns an OEMs post meta data `Object` if fields have been entered.
- `addToOEMArray(event)` - Adds an OEM `Array` to selectedOEMS `Array` if an oem is expanded.
- `updateDealer(event)` - Updates a dealers CTA link in the odin_dealers collection in MongoDB
- `getGMBDealers()` - Returns `Array` of odin_dealers that are filtered and mapped from the `oemGroup` array. Only dealers with organic_gmb_locations are returned.
- `removeFromOEMArray(event)` - Removes an OEM from the `selectedOEMS` array. This removes the expandable `OEMDealerSelect` card for the specified oem.

### OEMGroupCard.vue

**Description**
The OEM Group Card Component displays a list of OEMs as expandable cards. They expand into input fields for the user to enter meta data for local posts sheduling for any dealer they select within the OEM. 

**Tests**

**Props** 

- `OEMGroup` - An `Object` which contains the OEM ID (Name), and a list of dealers belonging to that oem.

**Data**

- `isCardExpanded` - A `Boolean` flag, used to determine if this card is expanded.
- `eventTitle` - A `String` value for the title of the local post.
- `summary` - A `String` value for the summary of the local post.
- `imageUrl` - A `String` value for the URL of the image for the local post.
- `dates` - An `Array` of `Date Objects`. The date range for the local post.
- `dealers` - An `Array` of odin_dealers that are filtered and mapped from the `oemGroup` array. Only dealers with organic_gmb_locations are returned.
- `selected` - A `Boolean` flag, used to determine if this oem has been selected
- `fields` - An `Object` to store all final values of inputted post details.
- `oemSet` - 
- `tooltipMessage` - A message that displays on top of the Event Title and Summary Fields.

**Methods**

- `fieldInput(event, key)` - Adds local post values into `fields` objects @input 
- `setIcon` - Just a flag to set appropriate icon for expanded and closed oem card
- `getGMBDealers` - Returns `Array` of dealers that are filtered and mapped from the dealers array `OEMGroup` object. Only dealers with `organic_gmb_locations` are returned.

### OEMDealerSelect.vue

**Description**


**Tests**

- Event Title `String` containing keyword DEALERSHIP and CITY should replace all with dealership name and city
- Summary `String` containing keyword DEALERSHIP and CITY should replace all with dealership name and city

**Props**
`oem` - An `Object` containing an `Array` of all dealerships pertaining to its OEM, and a `String` for its OEM name. 
`fields` - An `Object` containing inputted local post data from the `OEMGroupCard` component for `this` OEM. 

**Components**
`ScheduledPostsModal` - A modal which displays all live or scheduled future posts.

**Data**
`showMessage` - A `Boolean` value which controls whether or not a message is displayed under the CTA input field.
`utm` - An `Object` which holds UTM tags created by the UTM builder. This is appended to CTA Links. 
`modalActive` - A  `Boolean` value synced to the `ScheduledPostsModal` component, used to toggle display. 
`modalData` - An `Object` that's passed to the `ScheduledPostsModal`. Contains an `Array` of local posts for `this` OEM, and a title `String`.
`isOpen` - A `Boolean` value that toggles when the accordion is opened or closed. Used to set chevron icon.
`eventTitle` - A `String` value for the event title of the local post being scheduled.
`summary` - A `String` value for the summary of the local post being scheduled.
`imageUrl` - A `String` value for the image URL of the local post being scheduled. 
`dates` - An ``Array` of 2 dates which determine the start and end date for the uptime of the local post.
`openingModal` - A `Boolean` value used to activate loading icon when opening the `ScheduledPostsModal`.
`schedulingPosts` -  A `Boolean` value used to activate loading icon when sheduling posts.

**Computed**

`activeDealers` - Returns `activeOemDealers` array from user store. 

**Methods**

* `getOemDealers()` - Returns dealers `Array` of dealer `objects` from oem `prop`. Throws error if dealers is `undefined`.
* `getOemName()` - Returns oem `String` from oem `prop`. Throws error if `oem.oem` is `undefined`.
* `getDealerName(dealer)` - Returns dealer name `String` from a dealer `object`. Throws error if `dealer.dealer_business_name` is `undefined`.
* `getDealerOrganicLocations(dealer)` - Returns `Array` of organic locations from a dealer `object`. Throws error if `dealer.organic_gmb_oem_cta_link` is `undefined`.
* `getDealerCTALink(dealer)` -   Returns CTA URL `String` from dealer `object`. Throws error if `dealer.organic_gmb_oem_cta_link` is `undefined`.
* `getDealerSelectedLocations(dealer)` -  Returns `Array` of locations from dealer `object`. Throws error if `dealer.selectedLocations` is `undefined`.
* `getFields()` -  Returns fields `object` from `props`. Throws error if `fields` is `undefined`.
* `getEventTitleField()` - Returns event title `string` from fields `object`. Throws error if `fields.eventTitle` is `undefined`.
* `getSummaryField()` - Returns summary `string` from fields `object`. Throws error if `fields.summary` is `undefined`.
* `getImageUrlField()` - Returns image URL `string` from fields `object`. Throws error if `fields.imageUrl` is `undefined`.
* `getDatesField()` - Returns `Array` of `Dates` from fields `object`. Throws error if `fields.dates is `undefined`.
* `getUTMTags()` - Returns a `String` of UTM query parameters created from the utm `object`
* `setDefaultSelectedLocations()` - Iterates through list of dealers within `oem` prop, and adds locations to their `selectedLocations` `Array`. Adds them based on not having substrings in location name such as 'service/parts/collision'.
* `retrieveOEMDealers()` - Dispatches `action` in store to get all oem dealers. Used to refresh state after updating fields in `odin_dealers` collection.
* `checkIfActive(dealer)` - Checks if a dealer exists in `activeDealer` `Array`.
* `deactivateDealers()` - Turns off the 'Active' switch and removes dealer(s) from `activeDealers` `Array`.
* `setActive()` - Switch event handler, if turned on, fire `appendUTMS()`, if turned off, fire `deactivateDealers()`;
* `appendToActiveArray()` - Adds a dealer to `activeDealers` `Array` and dispatches an `action` in the `user` `store` to update `activeDealers` `state`.
* `openModal()` - This function loads scheduled and live posts for `this` oem and opens up the `ScheduledPostsModal` to display the data.` 
* `appendUTMS(dealer)` - Checks if dealer h s existing utm tags. if has it utm tags: replaces them with newest ones. else: appends new utm tags.
* `updateDealer(dealer)` - Appends utm tags from the UTM Builder and updates the dealer with the new CTA link in MongoDB.
* `schedulePosts()` - Gets all post data and attempts to schedule posts for each oem with selected locations
* `createLocalPost()` - Creates and returns a new local post object
* `replaceKeywords` - Used to replace certain keywords such as "DEALERSHIP" and "CITY" the dealerships name and city that`s currently being scheduled. 
* `snackbar()` - Opens a snackbar based on type passed in params
* `setIcon()` - Returns an icon as a string depending on if the card is expanded.