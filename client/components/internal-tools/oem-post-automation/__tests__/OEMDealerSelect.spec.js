import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils'
import OEMDealerSelect from 'components/internal-tools/oem-post-automation/OEMDealerSelect.vue'
import Buefy from 'buefy';
import { cloneDeep } from 'lodash' // Import cloneDeep for some deep clones
import { __createMocks as createStoreMocks } from 'store'

// Tell Jest to use the mock
// implementation of the store.
jest.mock('store');

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(Vuex);


describe('OEMDealerSelect', () => {
  let store;
  let wrapper;
  beforeEach(() => {
    // Create a fresh store and wrapper
    // instance for every test case.

    // Use cloneDeep to clone the store config before creating a store
    // to make sure we have a clean store in each test
    store = new Vuex.Store(cloneDeep(createStoreMocks()));
    wrapper = shallowMount(OEMDealerSelect, {
      store,
      localVue,
      propsData: {
        oem: {
          "dealers":
            [
              { "_id": "5e4ecf0152160257e08d3d40", "dealer_business_name": "Ens Toyota", "organic_gmb_locations": 
              [{ "name": "accounts/115154661579006199650/locations/18398452006424050873", "location_name": "Ens Toyota", "store_code": "ENS002", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "S7J 5L3", "administrativeArea": "Saskatchewan", "locality": "Saskatoon", "addressLines": ["627 Brand Court"] }, "primary_phone": "(306) 653-5611" }], "organic_gmb_oem_active": "__vue_devtool_undefined__", "organic_gmb_oem_cta_link": "https://www.enstoyota.com", "selectedLocations": [{ "name": "accounts/115154661579006199650/locations/18398452006424050873", "location_name": "Ens Toyota", "store_code": "ENS002", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "S7J 5L3", "administrativeArea": "Saskatchewan", "locality": "Saskatoon", "addressLines": ["627 Brand Court"] }, "primary_phone": "(306) 653-5611" }] },
              { "_id": "5e4ecf0152160257e08d3dcc", "dealer_business_name": "Myers Barrhaven Toyota", "organic_gmb_locations": 
              [{ "name": "accounts/115154661579006199650/locations/7484366003660891399", "location_name": "Myers Barrhaven Toyota", "store_code": "100009930", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "K2J 6H8", "administrativeArea": "ON", "locality": "Ottawa", "addressLines": ["4123 Strandherd Dr"] }, "primary_phone": "(613) 823-8088" }], "organic_gmb_oem_active": "__vue_devtool_undefined__", "organic_gmb_oem_cta_link": "https://www.myersbarrhaventoyota.ca/", "selectedLocations": [{ "name": "accounts/115154661579006199650/locations/7484366003660891399", "location_name": "Myers Barrhaven Toyota", "store_code": "100009930", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "K2J 6H8", "administrativeArea": "ON", "locality": "Ottawa", "addressLines": ["4123 Strandherd Dr"] }, "primary_phone": "(613) 823-8088" }] },
              { "_id": "5e4ecf0152160257e08d3ddb", "dealer_business_name": "Truro Toyota", "organic_gmb_locations": 
              [ { "name": "accounts/115154661579006199650/locations/3623717616295357565", "location_name": "Truro Toyota", "store_code": "TRU001", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "B2N 1C9", "administrativeArea": "NS", "locality": "Truro", "addressLines": ["310 Prince Street"] }, "primary_phone": "(902) 895-9000" },], "organic_gmb_oem_active": "__vue_devtool_undefined__", "organic_gmb_oem_cta_link": "https://www.trurotoyota.com/", "selectedLocations": [{ "name": "accounts/115154661579006199650/locations/17680840499522274258", "location_name": "Truro Toyota Service", "store_code": "TRU001S", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "B2N 1C9", "administrativeArea": "NS", "locality": "Truro", "addressLines": ["310 Prince St"] }, "primary_phone": "(902) 895-9000" }] },
              { "_id": "5e4ecf0152160257e08d3dfb", "dealer_business_name": "Summerside Toyota", "organic_gmb_locations": 
              [{ "name": "accounts/115154661579006199650/locations/570356917064478228", "location_name": "Summerside Toyota", "store_code": "SUM001", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "C1N 6V9", "administrativeArea": "PE", "locality": "Summerside", "addressLines": ["110 Walker Avenue"] }, "primary_phone": "(902) 436-5800" }], "organic_gmb_oem_active": "__vue_devtool_undefined__", "organic_gmb_oem_cta_link": "https://www.summersidetoyota.com/", "selectedLocations": [{ "name": "accounts/115154661579006199650/locations/347584816357861635", "location_name": "Summerside Toyota Parts", "store_code": "SUM001P", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "C1N 6V9", "administrativeArea": "PE", "locality": "Summerside", "addressLines": ["110 Walker Avenue"] }, "primary_phone": "(902) 436-5800" }] },
              { "_id": "5e4ecf0152160257e08d3e09", "dealer_business_name": "Rainbow Toyota", 
              "organic_gmb_locations": [{ "name": "accounts/115154661579006199650/locations/868191242183428054", "location_name": "Rainbow Toyota", "store_code": "RAI002", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "S9A 2X6", "administrativeArea": "SK", "locality": "North Battleford", "addressLines": ["Hwy 4 N", "3022 99 Street"] }, "primary_phone": "(306) 445-7799" }], "organic_gmb_oem_active": "__vue_devtool_undefined__", "organic_gmb_oem_cta_link": "https://www.rainbowtoyota.com/?utm_source=this&utm_medium=is&utm_campaign=just&utm_term=TEST&utm_content=jan1post", "selectedLocations": [{ "name": "accounts/115154661579006199650/locations/868191242183428054", "location_name": "Rainbow Toyota", "store_code": "RAI002", "address": { "regionCode": "CA", "languageCode": "en", "postalCode": "S9A 2X6", "administrativeArea": "SK", "locality": "North Battleford", "addressLines": ["Hwy 4 N", "3022 99 Street"] }, "primary_phone": "(306) 445-7799" }] }
            ],
          "oem": "Toyota"
        },
        fields: {
          "oem": "Toyota",
          "eventTitle": "Truck Month",
          "summary": "TRUCK MONTH",
          "dates": [new Date(), new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)],
          "imageUrl": "https://i.ytimg.com/vi/GpPJzRu3d38/maxresdefault.jpg",
        }
      }
    });
  }),

  test("Event Title string containing keyword DEALERSHIP and CITY should replace all with dealership name and city", async () => {
    //Setting eventTitle value
    wrapper.setData({
      eventTitle: "DEALERSHIP in CITY in DEALERSHIP in CITY",
      title: ""    
    });
       
    // Randomly selects a dealer out of the dealer list in oem props
    let dealer = wrapper.vm.oem.dealers[Math.floor(Math.random(wrapper.vm.oem.dealers.length))];
    let name = dealer.organic_gmb_locations[0].location_name;
    let city = dealer.selectedLocations[0].address.locality;
    
    // Run the keyword replacement function
    let replaced = wrapper.vm.replaceKeywords(dealer, dealer.selectedLocations[0]).eventTitle;
   
    await wrapper.vm.$nextTick();
    console.log(replaced);
    expect(replaced).toBe(name + " in " + city + " in " + name + " in " + city);
  });
  test("Summary string containing keyword DEALERSHIP and CITY should replace all with dealership name and city", async () => {
    //Setting summary value
    wrapper.setData({
      summary: "The DEALERSHIP is in CITY so come to DEALERSHIP in CITY",
    });
    
    // Randomly selects a dealer out of the dealer list in oem props
    let dealer = wrapper.vm.oem.dealers[Math.floor(Math.random(wrapper.vm.oem.dealers.length))];
    let name = dealer.organic_gmb_locations[0].location_name;
    let city = dealer.selectedLocations[0].address.locality;
    
    // Run the keyword replacement function
    let replaced = wrapper.vm.replaceKeywords(dealer, dealer.selectedLocations[0]).summary;
    
    await wrapper.vm.$nextTick();
    console.log(replaced);
    expect(replaced).toBe("The " + name + " is in " + city + " so come to " + name + " in " + city);
  });
  
  test("Card icon should be correct when card is expanded or collapsed", () => {
    // Set boolean and run switch function
    wrapper.vm.isCardExpanded = true;
    expect(wrapper.vm.setIcon()).toBe("chevron-down");
    wrapper.vm.isCardExpanded = false;
    expect(wrapper.vm.setIcon()).toBe("chevron-left");
  });

  test("UTM tags will APPEND onto naked CTA Link on update", async () => {
    // Select dealer without UTM tags in CTA link 
    let dealer = wrapper.vm.oem.dealers[Math.floor(Math.random(wrapper.vm.oem.dealers.length - 1))]
    wrapper.vm.appendUTMS(dealer);

    wrapper.vm.$nextTick();
    expect(dealer.organic_gmb_oem_cta_link.includes("utm")).toBeTruthy();
  });

  test("Should render '.error-message' element if fails to get an oem prop", async () => {
    // Set oem object to undefined
    wrapper.setProps({oem: undefined});
    wrapper.vm.$nextTick();
    expect(wrapper.exists('.error-message')).toBeTruthy();
  });

  // test("Local Post object is created properly", async () => {
  //   let dealer = wrapper.vm.oem.dealers[Math.floor(Math.random(wrapper.vm.oem.dealers.length))];

  //   let eventTitle = wrapper.vm.replaceKeywords(dealer, dealer.selectedLocations[0]).eventTitle;
  //   let summary = wrapper.vm.replaceKeywords(dealer, dealer.selectedLocations[0]).summary;
  //   let image = wrapper.vm.fields.imageUrl;
  //   let dates = wrapper.vm.fields.dates;
  //   let oem = wrapper.vm.oem.oem
  //   let selectedLocation = dealer.selectedLocations[0];

  //   wrapper.vm.$nextTick();
  //   expect(wrapper.exists('')).toBeTruthy();
  // });

});


