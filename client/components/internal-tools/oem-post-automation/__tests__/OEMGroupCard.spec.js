import Vuex from 'vuex';
import { mount } from '@vue/test-utils'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import { __createMocks as createStoreMocks } from 'store'
import OEMGroupCard from 'components/internal-tools/oem-post-automation/OEMGroupCard.vue'
import Vue from 'vue'
import { cloneDeep } from 'lodash' // Import cloneDeep for some deep clones

//  These css framework elements need to be ignored in jest
Vue.config.ignoredElements = ['b-tooltip', 'b-field', 'b-autocomplete', 'b-button', 'b-switch', 'b-table', 'b-table-column', 'b-input', 'b-collapse', 'b-datepicker', 'b-icon']

// Tell Jest to use the mock
// implementation of the store.
jest.mock('store');

const localVue = createLocalVue();

localVue.use(Vuex);

describe('OEMGroupCard', () => {
    let store;
    let wrapper;
    
    beforeEach(() => {
        // Create a fresh store and wrapper
        // instance for every test case.
        store = new Vuex.Store(cloneDeep(createStoreMocks()))
        wrapper = shallowMount(OEMGroupCard, {
            store: store,
            localVue,
            propsData: {
                OEMGroup: { "_id": "Chrysler", "dealers": [{ "_id": "5d9d1e1cefabce27543e267b", "dealer_business_name": "Lambton Chrysler" }] }
            }
        });
    }),

    
    test("Card icon should be correct when card is expanded or collapsed", () => {
        wrapper.vm.isCardExpanded = true;
        expect(wrapper.vm.setIcon()).toBe("chevron-down");
        wrapper.vm.isCardExpanded = false;
        expect(wrapper.vm.setIcon()).toBe("chevron-left");
    });

    test("Card icon should be a checkmark when selected", () => {
        wrapper.vm.selected = true;
        expect(wrapper.vm.setIcon()).toBe("check");
    })
});