# Campaign Generation



## Components

### CampaignGenerator.vue

**Description**

The main component that holds the campaign generator workflow

**Components**

- EmailListUploadCard
- CampaignConfigurationCard



### EmailListUploadCard.vue

**Description**

The widget responsible for handling the uploading of .xlsx files to the Campaign Generator. 

**Tests Implemented**

- The validation button should be disabled if no email list is uploaded

**Test Ideas**

- Clicking the "Download Template" button should download the `.xlsx` template
  - Todo: The test should programmatically download the file - need to develop a strategy to get this working.

##### Components

- `DragAndDropUploader` - Used for uploading files
- `EmailListEditorTable` - Used inside popup modal during validation

**Data**

- `file` - A `File` type object which is uploaded to the component from the drag-and-drop widget
- `validated` - The results of the email list validation. It is an `Object` which contains the following format: `{ json: [ {<Email Row Object>} ], errors: [ {<Error Object>} ] }`
- `isComponentModalActive` - The `Boolean` flag for activating the `EmailListEditorTable` component in a modal
-  `error` - The standard error flag `Boolean` used to switch off the component

**Watch**

- `validated(newValue)` - Watches the `validated` variable and triggers the `isComponentModalActive` flag if the validation is completed. It should set the flag back to false if `validated` is undefined.
- `isComponentModalActive` - Deletes the attached file when the variable is false - the modal is deactivated

**Methods**

- `async downloadEmailTamplate()` - Dispatches the `mailchimp/downloadEmailTemplate` action which makes the API call to the Campaign Management service to retrieve and immediately download the template file as an attachment.
- `async uploadTemplate()` - Uploads an `xlsx` file to the Campaign Management service and completes the validation of the email list inside. It should return the validation object as a response.
- `attachFile(file)` - Attaches the uploaded file (helper function)
- `deleteFile(file)` - Deletes the uploaded file (helper function) 



### EmailListEditorTable.vue

**Description**

Displays all email records that were uploaded in the template file. The component allows for visual error display and manual editing of the email list before it is uploaded to the service.

**Tests Implemented**

- Render the table if the `validated` payload is not undefined, otherwise render the error message.

**Test Ideas**

- Table should fail gracefully if data is not formatted correctly
- Submit button should be disabled if audience is not selected

**Components**

- `TableCellField` - This child component is not currently being used. The idea is that future versions of the table widget could have a generic field which when passed a prop for `value` it can display some responsive client side validation on the `b-input` element inside. 

**Data**

- `selected` - The row selected in the table - currently not used for anything
- `error` - The standard error flag `Boolean` used to switch off the component
- `audiences` - The list of audiences as an `Array`
- `selectedAudience` - The currently seleceted audience `Object`
- `loading` - The `Boolean` loading status for the toast

**Props**

- `validated` - The results of the email list validation. It is an `Object` which contains the following format: `{ json: [ {<Email Row Object>} ], errors: [ {<Error Object>} ] }`

**Watch**

- `validated(newValue)` - `validated` is watched and triggers the error state when it is `undefined`. It also selects the first element in the result as the default `selected` item in the table.
- `audiences(newValue)` - `audiences` is watched and triggers the error state when it is `undefined`.

**Methods**

- `async getAudiences()` - Returns a list of audiences from Mailchimp by dispatching the `mailchimp/getAudiences` action
- `uploadEmails()` - Uploads the JSON payload of emails to Mailchimp by dispatching the `mailchimp/uploadEmails` action 
- `openToast(response)` -     * Opens a toast on the bottom center of the scree its input is the response object from the mailchimp/updateAudience action



### CampaignConfigurationCard.vue

**Description**

Handles the setup of new campaigns

**Tests Implemented**

None

**Test Ideas**

None

**Components**

- CampaignTimeFields

- CampaignTemplatesFields

- CampaignAudienceFields

- CampaignSettingsFields

**Data**

- `error` - The standard error flag `Boolean` used to switch off the component
- `loading` - Displays the button loading animation `Boolean`

- `selected` - Contains the values emitted from the child components for the scheduling payload `Object`

  - ```
    - {
    	audienceId: String,
    	batchSize: Int,
    	email: String,
    	name: String,
    	templateId: String,
    	mergeFields: [],
    	title: String,
    	times: String,
    	segments: [],
    	subject: String,
    }
    ```

**Computed**

- `disableScheduleButton` - TODO - Only allow the button to be clicked when the selected fields are all set and validated

**Methods**

- `setSelectedTemplates(event)` - Sets payload fields from the `event` object
- `setSelectedTemplateFields(event)`- Sets payload fields from the `event` object
- `setSelectedTimes(event)`- Sets payload fields from the `event` object
- `setSelectedAudiences(event)`- Sets payload fields from the `event` object
- `setSelectedSettings(event) - Sets payload fields` from the `event` object
- `schedule()` - Schedules the campaigns using the `selected` `Object`

### CampaignTemplateFields.vue

**Description**

A set of reactive fields that hold the campaign generation template like the template, name, contact, logo

**Test Implemented**

- Render the widget if the `templates` payload is not undefined, otherwise render the error message.
- Changing the `selected` object emits the `selectedSettingsUpdated` event - TODO fix emit error on test
- Changing the `settings` object emits the `templateFieldsUpdated` event - TODO fix emit error on test

**Test Ideas**

**Components**

None

**Data**

- `settings` - The default settings which are preserved in the database for each dealer

  - ```
    {
    	dealer_name: { tag: "undefined", value: "" },
    	website_url: { tag: "undefined", value: "" },
    	contact_url: { tag: "undefined", value: "" },
    	logo_url: { tag: "undefined", value: "" },
    	colour: { tag: "undefined", value: "" }
    },
    ```

- `selected` - An `Object` that holds the field values for:

  - ```{
    {
      template: Object
    }
    ```

- `template` - The currently selected template

- `error` - The standard error flag `Boolean` used to switch off the component

**Props**

None

**Computed**

`dealer_id` - The odin dealer dealer Id from the Vuex store

**Watch**

- `templates(newValue)` - Watches for changes to the list of templates from the `campaign-mangement` service with the `newValue` 
- `selected(newValue)` - Emits an `selectedTemplatesUpdated` event with the `newValue` to the parent component when the template is changed.
- `settings(newValue)` - Emits a `templateFieldsUpdated` event  with the `newValue` to the parent component when the template fields are changed.

**Mounted**

- Calls the `getCampaignTemplates` and `getTemplateSettings` actions from the store service handler via the API

**Methods**

- `getCampaignTemplates` - Calls the vuex action to get the campaign template payload from the service
- `getTemplateSettings` - Calls the vuex actio to get hte campaign template payload from the service
- `updateTemplateSettings` - TODO - updates the template settings
- `getTemplate` - TODO - will get the template data (useful when we want to preview the template in the UI)



### CampaignTimeFields.vue

**Description**

A set of reactive fields that hold the campaign time settings this includes setting the schedule for all campaigns to run for each day of the week

**Test Implemented**

- Changing the selected object emits the `selectedTimesUpdated` event

**Test Ideas**

- Ensure `addEntry` computes a correct entry for the table
- Ensure next time computes the correct next 15 minute interval

**Components**

None

**Data**

- `startDate` -  Defaults to be a `new Date()` object for the current day

- `newDay` - The default item in the day selector, it's the `String` 'Monday'

- `minutesGranularity` - Default interval for the time selector for 15 minutes (as per mailchimp)

- `   newTime` -  The new time that will be set to the next 15 minute period from now when the component is mounted - it's always either on the 0, 15, 30, 45 minute

- `columns` - The structure for the time table element - it just has a field for every day of the week

- `selected` - Holds all the times in a list defined for the campaigns, will be emitted to the parent component

  - ``` 
    {
    	times: [],
    }
    ```

- `error` - The standard error flag `Boolean` used to switch off the component

**Props**

None

**Mounted**

- Assigned the value of `nextTime` to `newTime` effectively setting the next valid 15 minute interval to schedule a campaign

**Watch**

- `selected` - Emits the `selectedTimesUpdatedevent` to the parent component when the times change
  - Also has business logic to format the times into a nice `HH:MM` string. TODO - probably should live in it's own function 

**Computed**

- `nextTime` - Computes the next available time for the time selector
  - `get` - returns the next available time to display in the selector v-model
  - `set(newValue)` - updates the `newTime` variable with the next available time

**Methods**

- `addEntry` - Adds a new time entry to the specified day of week in the `selected.times` array

### CampaignSettingsFields.vue

**Description**

A set of reactive fields that hold the campaign generation settings like the subject, from, to and title

**Test Implemented**

- Changing the selected object emits the `selectedSettingsUpdated` event

**Test Ideas**

**Components**

None

**Data**

- `selected` - An `Object` that holds the field values for:

  - ```{
    {
      subject: String
      name: String
      email: String
      title: String
    }
    ```

    ​    

- `error` - The standard error flag `Boolean` used to switch off the component

**Props**

None

**Watch**

- `selected` - Emits the `selectedSettingsUpdated` event to the parent component

**Methods**

None