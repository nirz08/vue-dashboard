
//  Import your component and setup vue.test.utils
import Component from 'components/campaign-generator/EmailListEditorTable.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils';
const localVue = createLocalVue()

//  Handle custom css frameworks
import Buefy from 'buefy'
localVue.use(Buefy)

//  Setup and configure the mock store
import Vuex from 'vuex';
import { __createMocks as createStoreMocks } from 'store'
jest.mock('store');
localVue.use(Vuex);
// Used for cloning the store and all its nested keys into the mock
import { cloneDeep } from 'lodash' 

//  Tests in here. Did you remember to name your component?
describe(Component.name, () => {

  // test("Mounts properly", () => {
  //   expect(wrapper.isVueInstance()).toBeTruthy();
  // });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

  test('Render the table if the `validated` payload is not undefined, otherwise render the error message.', async () => {
    const validated = {
      json: [{ "Email": "nirzinger@test.com", "First Name": "Noah", "Last Name": "Irzinger", "Sales/Service": "Sales"}],
      errors: []
    }
    
    const wrapper = shallowMount(Component, {
      localVue,
      propsData: { validated },
      store: new Vuex.Store(cloneDeep(createStoreMocks())),
      data() {
        return {
          audiences: {
            lists: []
          }
        }
      }
    })
    // wrapper.setData({audiences: {lists: []}}) - this doesn't work, instead set the data inside the wrapper init
    expect(wrapper.vm.error).toBe(false)
    wrapper.setProps({validated: undefined })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.error).not.toBe(false)
  });

  test('Display the submit button if there is no errors', async () => {
    const validated = {
      json: [{ "Email": "nirzinger@test.com", "First Name": "Noah", "Last Name": "Irzinger", "Sales/Service": "Sales"}],
      errors: []
    }
    const wrapper = shallowMount(Component, {
      localVue,
      propsData: { validated },
      store: new Vuex.Store(cloneDeep(createStoreMocks())),
      data() {
        return {
          audiences: { lists: [] }
        }
      }
    })
    expect(wrapper.vm.error).toBe(false)
    let submitButton = wrapper.find('#submit-emails-button')
    expect(submitButton.exists()).toBe(true)
    wrapper.setProps({validated: { json:[], errors: [ {"an":"error"} ] } } )
    await wrapper.vm.$nextTick()
    expect(submitButton.exists()).toBe(false)
  });
  
  test('The submit button should be disabled if no audience is selected', async () => {
    const validated = {
      json: [{ "Email": "nirzinger@test.com", "First Name": "Noah", "Last Name": "Irzinger", "Sales/Service": "Sales"}],
      errors: []
    }
    const wrapper = shallowMount(Component, {
      localVue,
      propsData: { validated },
      store: new Vuex.Store(cloneDeep(createStoreMocks())),
      data() {
        return {
          audiences: { lists: [] }
        }
      }
    })
    let button = wrapper.find('#submit-emails-button')
    expect(button.attributes('disabled')).toBe('true')
    wrapper.setData({selectedAudience: "It's me! :)"})
    await wrapper.vm.$nextTick()
    button = wrapper.find('#submit-emails-button')
    expect(button.attributes('disabled')).toBeUndefined()
  });

});