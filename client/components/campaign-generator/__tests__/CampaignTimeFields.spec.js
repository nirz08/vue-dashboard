
//  Import your component and setup vue.test.utils
import Component from 'components/campaign-generator/CampaignTimeFields.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils';
const localVue = createLocalVue()

//  Handle custom css frameworks
import Buefy from 'buefy'
localVue.use(Buefy)

//  Tests in here. Did you remember to name your component?
describe(Component.name, () => {

  test("Mounts properly", () => {
    const wrapper = shallowMount(Component, {
      localVue,
    })
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

  // test("Changing the selected object emits the `selectedTimesUpdated` event", async () => {
  //   const wrapper = shallowMount(Component, {
  //     localVue,
  //   })
  //   const selected = {
  //     times: [
  //       {
  //         Monday: new Date()
  //       }
  //     ]
  //   }
  //   wrapper.setData({selected: selected})
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.emitted().selectedTimesUpdated).toStrictEqual([[{
  //     selected: selected
  //   }]])
  // });

});