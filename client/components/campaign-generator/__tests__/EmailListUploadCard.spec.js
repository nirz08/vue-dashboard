
import { shallowMount, createLocalVue } from '@vue/test-utils';
import Component from 'components/campaign-generator/EmailListUploadCard.vue'
import Buefy from 'buefy'
const localVue = createLocalVue()
localVue.use(Buefy) //  handle css framework elements

//  Did you remember to name your component?
describe(Component.name, () => {

  // test("Mounts properly", () => {
  //   expect(wrapper.isVueInstance()).toBeTruthy();
  // });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

//   test('Clicking the "Download Template" button should download the `.xlsx` template', async () => {
    //TODO
//   });

  test('The validation button should be disabled if no email list is uploaded', async () => {
    const wrapper = shallowMount(Component, {
      localVue,
    })
    let button = wrapper.find('#validate-emails-button')
    expect(button.attributes('disabled')).toBe('true')
    wrapper.setData({file: "A file :)"})
    await wrapper.vm.$nextTick()
    button = wrapper.find('#validate-emails-button')
    expect(button.attributes('disabled')).toBeUndefined()
  });

});