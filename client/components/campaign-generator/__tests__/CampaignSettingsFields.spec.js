
//  Import your component and setup vue.test.utils
import Component from 'components/campaign-generator/CampaignSettingsFields.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils';
const localVue = createLocalVue()

//  Handle custom css frameworks
import Buefy from 'buefy'
localVue.use(Buefy)
import Vuex from 'vuex';
import {
  __createMocks as createStoreMocks
} from 'store'
jest.mock('store');
localVue.use(Vuex);
// Used for cloning the store and all its nested keys into the mock
import {
  cloneDeep
} from 'lodash'

//  Tests in here. Did you remember to name your component?
describe(Component.name, () => {

  // test("Mounts properly", () => {
  //   expect(wrapper.isVueInstance()).toBeTruthy();
  // });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

  test("Changing the selected object emits the `selectedSettingsUpdated` event", async () => {
    const wrapper = shallowMount(Component, {
      localVue,
      store: new Vuex.Store(cloneDeep(createStoreMocks())),
    })
    const selected = {
      subject: 'this',
        name: 'is',
        email: 'a',
        title: 'test',
    }
    wrapper.setData({selected: selected})
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted().selectedSettingsUpdated).toStrictEqual([[{
      subject: 'this',
        name: 'is',
        email: 'a',
        title: 'test',
    }]])
  });

});