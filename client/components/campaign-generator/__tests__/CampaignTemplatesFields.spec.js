
//  Import your component and setup vue.test.utils
import Component from 'components/campaign-generator/CampaignTemplatesFields.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils';
const localVue = createLocalVue()

//  Handle custom css frameworks
import Buefy from 'buefy'
localVue.use(Buefy)
import Vuex from 'vuex';
import {
  __createMocks as createStoreMocks
} from 'store'
jest.mock('store');
localVue.use(Vuex);
// Used for cloning the store and all its nested keys into the mock
import {
  cloneDeep
} from 'lodash'

//  Tests in here. Did you remember to name your component?
describe(Component.name, () => {

  // test("Mounts properly", () => {
  //   expect(wrapper.isVueInstance()).toBeTruthy();
  // });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

  test('Render the widget if the `templates` payload is not undefined, otherwise render the error message.', async () => {
    
    const wrapper = shallowMount(Component, {
      localVue,
      store: new Vuex.Store(cloneDeep(createStoreMocks())),
      data() {
        return {
          templates: ["i'm an template!"]
        }
      }
    })
    expect(wrapper.vm.error).toBe(false)
    wrapper.setData({templates: undefined})
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.error).not.toBe(false)
  });

  // test("Changing the `selected` Object emits the `selectedTemplatesUpdated` event", async () => {
  //   const wrapper = shallowMount(Component, {
  //     localVue,
  //   })
  //   const selected = {
  //     template: {}
  //   }
  //   wrapper.setData({selected: selected})
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.emitted().selectedTemplatesUpdated).toStrictEqual([
  //     {template: {}}
  //   ])
  // });

  // test("Changing the settings object emits the `templateFieldsUpdated` event", async () => {
  //   const wrapper = shallowMount(Component, {
  //     localVue,
  //   })
  //   const settings = {
  //     dealer_name: { tag: "1", value: "1" },
  //     website_url: { tag: "2", value: "2" },
  //     contact_url: { tag: "3", value: "3" },
  //     logo_url: { tag: "4", value: "4" },
  //     colour: { tag: "5", value: "5" }
  //   }
  //   wrapper.setData({settings: settings})
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.emitted().templateFieldsUpdated).toStrictEqual([[{
  //     settings: {
  //       dealer_name: { tag: "1", value: "1" },
  //       website_url: { tag: "2", value: "2" },
  //       contact_url: { tag: "3", value: "3" },
  //       logo_url: { tag: "4", value: "4" },
  //       colour: { tag: "5", value: "5" }
  //     }
  //   }]])
  // });

});