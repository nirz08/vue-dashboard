
//  Import your component and setup vue.test.utils
import Component from 'components/campaign-generator/CampaignAudienceFields.vue'
import { shallowMount, createLocalVue } from '@vue/test-utils';
const localVue = createLocalVue()

//  Handle custom css frameworks
import Buefy from 'buefy'
localVue.use(Buefy)

//  Tests in here. Did you remember to name your component?
describe(Component.name, () => {

  // test("Mounts properly", () => {
  //   expect(wrapper.isVueInstance()).toBeTruthy();
  // });

  // test("Renders properly", () => {
  //   expect(wrapper.html()).toMatchSnapshot();
  // });

  test('Render the widget if the `audiences` payload is not undefined, otherwise render the error message.', async () => {
    
    const wrapper = shallowMount(Component, {
      localVue,
      data() {
        return {
          audiences: ["i'm an audience!"]
        }
      }
    })
    expect(wrapper.vm.error).toBe(false)
    wrapper.setData({audiences: undefined})
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.error).not.toBe(false)
  });

  // test("Changing the audience emits the `selectedAudienceUpdated` event", async () => {
  //   const wrapper = shallowMount(Component, {
  //     localVue,
  //   })
  //   const selected = {
  //     batchSize: 40,
  //     audienceId: "123",  
  //     segments: ["segment1", "segment2"],
  //     numberOfCampaigns: 0,
  //     numberOfMembers: 0,
  //   }
  //   wrapper.setData({selected: selected})
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.emitted().selectedAudienceUpdated).toEqual([[selected]])
  // });

});