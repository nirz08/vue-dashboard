'use strict'

const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')
const TerserPlugin = require('terser-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
require('dotenv').config({config: path.resolve(__dirname, './config/.env')});
var config = {
  mode: process.env.NODE_ENV,
  entry: [
    'babel-polyfill',
    './src/client/main.js'
  ],
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    chunkFilename: 'js/blob.[id].js',
    filename: 'js/blob.js'
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src/client'),
      '@api': path.resolve(__dirname, 'src/client/api'),
      'assets': path.resolve(__dirname, 'src/client/assets'),
      'components': path.resolve(__dirname, 'src/client/components'),
      '@config': path.resolve(__dirname, 'src/client/config'),
      '@router': path.resolve(__dirname, 'src/client/router'),
      'store': path.resolve(__dirname, 'src/client/store'),
      '@styles': path.resolve(__dirname, 'src/client/styles'),
      '@utils': path.resolve(__dirname, 'src/client/utils'),
      '@views': path.resolve(__dirname, 'src/client/views'),
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  node: {
    fs: "empty"
  },
  module: {
    rules: [
      {
        test: /\.vue.*$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: [
              'vue-style-loader',
              'css-loader',
              'sass-loader'
            ],
            sass: [
              'vue-style-loader',
              'css-loader',
              'sass-loader?indentedSyntax'
            ]
          }
        }
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread']
          }
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          'style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader', { loader: 'sass-resources-loader', options: { resources: [path.join(__dirname, '/src/client/styles/_variables.scss'), path.join(__dirname, '/src/client/styles/_global.scss')] } }
        ]
      }, {
        test: /\.(png|svg|eot|woff|ttf|svg|woff2)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: "[path][name].[ext]"
            }
          }
        ]
      }
    ]
  },
}

if ('production' == process.env.NODE_ENV) {
  config.plugins = [
    //new BundleAnalyzerPlugin(),
    new Dotenv({path: './config/.env'}),
    new VueLoaderPlugin(),
    new TerserPlugin(),
    new HTMLWebpackPlugin({
      template: path.resolve(__dirname, './index.html'),
      filename: 'index.html',
      chunksSortMode: 'dependency'
    })
  ]
  config.optimization = {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    },
    minimizer: [new TerserPlugin({
      parallel: true,
      terserOptions: {
        ecma: 6,
      },
    })]
  }
} else {
  config.entry.push('webpack-hot-middleware/client')
  config.plugins = [
    new Dotenv({path: './config/.env'}),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new VueLoaderPlugin(),
    new HTMLWebpackPlugin({
      template: path.resolve(__dirname, './index.html'),
      filename: 'index.html'
    })
  ]
}

module.exports = config