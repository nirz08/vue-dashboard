FROM dacvrtus/docker-node-awscli:latest

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

RUN npm install pm2 -g

COPY package*.json ./
RUN npm install
COPY . ./
ARG NODE_ENV
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION
ARG AWS_S3_BUCKET
ARG AWS_S3_PREFIX_CONFIG
RUN mkdir config

RUN AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION BUCKET=$AWS_S3_BUCKET PREFIX=$AWS_S3_PREFIX_CONFIG node deploy.js

RUN if [ "$NODE_ENV" = "production" ] || [ "$NODE_ENV" = "development" ] ; then \
        npm run build ; \
    else \
        npm run test-pipelines ; \  
    fi ;

RUN if [ $? = 1 ] ; then exit 1 ; fi

ENTRYPOINT ["npm","start"]

EXPOSE 80