module.exports = {
    /**
     * Non-blocking sleep function
     * @param {Number} milliseconds - The time in miliseconds to delay for 
     */
    sleep: (milliseconds) => {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
    },

    /**
     * Synchronize a local directory with AWS S3 using the host OS aws cli implementation
     * @param {string} remoteDir - the s3 bucket path
     * @param {string} localDir - the config folder path
     * @returns {EventEmitter} e - The emitter to listen for events from 
     */
    synchronizeS3: (remoteDir, localDir) => {
        const { exec } = require('child_process')
        const emitter = require('events').EventEmitter;
        let e = new emitter()
        //  Async call to child_process
        exec(`aws s3 sync ${remoteDir} ${localDir}`, { maxBuffer: 1024 * 500 }, (error, stdout, stderr) => {
            //  return a failed event with the error instead of throwing
            if (error) {
                e.emit('error', error)
            }
            e.emit('SynchronizedS3', stdout)
        })
        // Synchronous return of emitter to register listeners
        return e
    }
}

