module.exports = {
    collectCoverage: false,
    collectCoverageFrom: [
        "**/*.{js,vue}",
        "!**/node_modules/**"
      ],
      coveragePathIgnorePatterns: [
        "node_modules",
        "test-config",
        "interfaces",
        "__mocks__"
    ],
    coverageReporters: [
        "html",
        "text-summary"
    ],
    roots: [
        "<rootDir>"
    ],
    modulePaths: [
        "<rootDir>"
    ],
    moduleFileExtensions: [
        "js",
        "json",
        "vue"
    ],
    moduleDirectories: [
        "node_modules",
        "src"
      ],
    moduleNameMapper: {
        "components/(.*)$": "<rootDir>/src/client/components/$1",
        "assets/(.*)$": "<rootDir>/src/client/assets/$1",
        "store(.*)$": "<rootDir>/src/client/store/$1",
        "^vue$": "vue/dist/vue.common.js",
        ".\\.(css|styl|less|sass|scss)$": "<rootDir>/fileTransformer.js"
    },
    transform: {
        ".*\\.(vue)$": "vue-jest",
        "^.+\\.js$": "babel-jest",
        "\\.svg$": "<rootDir>/fileTransformer.js",
        ".\\.(css|styl|less|sass|scss)$": "<rootDir>/fileTransformer.js"
    },
    verbose: true,
}