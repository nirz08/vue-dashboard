var fs = require('fs')
var AWS = require('aws-sdk')
if(!process.env.AWS_ACCESS_KEY_ID) {
    require('dotenv').config()
}
AWS.config.update({ accessKeyId: process.env.AWS_ACCESS_KEY_ID, secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, region: process.env.AWS_DEFAULT_REGION })
var s3 = new AWS.S3()

var environment = process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'test' ? 'dev' : 'prod'

s3.listObjects({ Bucket: process.env.BUCKET, Delimiter: '/', Prefix: 'dealerportal/' + environment + '/config/'}, (err, data) => {
    for (let x of data.Contents) {
        var filename = x.Key.split('/')
        filename = filename[filename.length - 1]
        if (filename) {
            console.log(filename)
            var filestream = fs.createWriteStream('./config/' + filename)
            var s3Stream = s3.getObject({ Bucket: process.env.BUCKET, Key: x.Key }).createReadStream();

            s3Stream.on('error', (err) => {
                // NoSuchKey: The specified key does not exist
                console.log(err);
            });

            s3Stream.pipe(filestream).on('error', (err) => {
                console.log(err)
            }).on('close', () => {
                console.log("processed file")
            })
        }
    }
    if(err) console.log(err)
})

